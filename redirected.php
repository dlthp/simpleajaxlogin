<?php
session_start();

if(!isset($_SESSION['user_session']))
{
	header("Location: index.php");
}

include_once 'dbconfig.php';

$stmt = $db->prepare("SELECT * FROM tbl_users WHERE user_id=:uid");
$stmt->execute(array(":uid"=>$_SESSION['user_session']));
$row=$stmt->fetch(PDO::FETCH_ASSOC);

$users = $db->prepare("SELECT * FROM tbl_users");
$users->execute();
$result = $users->fetchAll();
?>

<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Form using jQuery Ajax and PHP MySQL</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="style.css" rel="stylesheet" media="screen">
</head>

<body>
   

<div class="container small-container">
    <h2>Hi '<?php echo $row['user_name']; ?>' Welcome to the Admin page.
    <a href="logout.php" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> &nbsp;Sign Out</a></li>
    </h2>
    <hr>
    <h3>Available Users <button class="btn btn-primary" onClick="addUser();">Add User</button> </h3>
    <table class="table table-striped">
        <thead>
            <tr>
            <td>ID</td>
            <td>User Name</td>
            <td>Email</td>
            <td>DOB</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($result as $user){ ?>
            <tr>
                <td><?php echo $user["user_id"] ?></td>
                <td><?php echo $user["user_name"] ?></td>
                <td><?php echo $user["email"] ?></td>
                <td><?php echo $user["dob"] ?></td>
                <td>
                    <button class='btn-primary' tooltip="Edit" onclick="userdetail(<?php echo $user['user_id'] ?>);"><i class='fa fa-pencil'></i></button>
                    <button class='btn-danger' tooltip="Delete" onclick="userdelete(<?php echo $user['user_id'] ?>);"><i class='fa fa-trash'></i></button>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>

    <hr>
        <form class='form form-horizontal d-none' id='user-form'>
            <div class="form-group row d-none">
                <label class="col-sm-2 col-form-label ">Id</label>
                <div class="col-sm-10">
                    <input type="text" name='user_id' id='user_id' class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" name='user_name' id='user_name' class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <input type="text" name='user_password' id='user_password' class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" name='email' id='email' class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">DOB</label>
                <div class="col-sm-10">
                    <input type="text" name='dob' id='dob' class="form-control">
                </div>
            </div>
            <div class='form-group row'>
                <div class=col-sm-12>
                    <button class='btn btn-success' name='sbmit' class="float-right">Save</button>
                </div>
            </div>
        </form>
</div>

<script src="https://use.fontawesome.com/ee309940e2.js"></script>
<script type="text/javascript" src="script.js"></script>
</body>
</html>