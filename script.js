
$('document').ready(function()
{ 
/* validation */
$("#login-form").validate({
rules:
{
password: {
required: true,
},
user_email: {
required: true,
email: true
},
},
messages:
{
password:{
required: "please enter your password"
},
user_email: "please enter your email address",
},
submitHandler: submitForm	
});  
/* validation */

/* login submit */
function submitForm()
{		
var data = $("#login-form").serialize();

$.ajax({

type : 'POST',
url  : 'login_process.php',
data : data,
beforeSend: function()
{	
$("#error").fadeOut();
$("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
},
success :  function(response)
{						
        if(response=="ok"){

                $("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Signing In ...');
                setTimeout(' window.location.href = "redirected.php"; ',1000);
        }
        else{

                $("#error").fadeIn(1000, function(){						
$("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
                                                        $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
                                        });
        }
}
});
return false;
}
/* login submit */
});

function addUser(){
        $('#user-form').removeClass('d-none');
}

function userdetail(user_id){
        $('#user-form').removeClass('d-none');
        $.ajax({

                type : 'POST',
                url  : 'user_detail.php',
                data : { user_id : user_id},
                success :  function(response)
                {	
                        var obj = jQuery.parseJSON(response);
                        $('#user_id').val(obj[0].user_id);
                        $('#user_name').val(obj[0].user_name);
                        $('#user_password').val(obj[0].user_password);
                        $('#email').val(obj[0].email);
                        $('#dob').val(obj[0].dob);
                }
                });
                return false;
}

$('#user-form').submit(function(event){
        event.preventDefault();
        $.ajax({

                type : 'POST',
                url  : $('#user_id').val() > 0 ? 'user_edit.php' : 'user_add.php',
                data : $('#user-form').serialize(),
                success :  function(response)
                {	
                        if(response=="ok"){
                                alert("Successfully Added/Updated user.");
                                location.reload();
                        }
                        else{
                                alert("Failed to Added/Updated user.");
                        }  
                }
                });
                return false;
});

function userdelete(user_id){
        var r = confirm("Are you sure to delete the user");
        if (r == true) {
                $.ajax({

                type : 'POST',
                url  : 'user_delete.php',
                data : { user_id : user_id},
                success :  function(response)
                {						
                        if(response=="ok"){
                                alert("Successfully removed user.");
                                location.reload();
                        }
                        else{
                                alert("Failed to delete user.");
                        }
                }
                });
                return false;
        }
}